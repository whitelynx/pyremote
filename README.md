PyRemote
========

A simple IR remote app using [LIRC][] and [Kivy][].

[LIRC]: https://www.lirc.org/
[Kivy]: https://kivy.org/


Prerequisites
-------------

- [Pipenv](https://pipenv.pypa.io/en/latest/index.html)


Usage
-----

```bash
pipenv install
pipenv run python pyremote.py
```

You can adjust the remote and buttons by editing `pyremote.kv`.


License
-------

PyRemote is released under the terms of the MIT License; see [LICENSE]() for more info.
