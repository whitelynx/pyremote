import lirc

import kivy
kivy.require('1.0.7')

from kivy.app import App
from kivy.clock import Clock
from kivy.properties import StringProperty
from kivy.uix.button import Button
from kivy.uix.label import Label


LIRC = lirc.Client()

TOUCH_EVENT_ALLOW_LIST = ('event7',)


class PyRemoteApp(App):
    def toast_error(self, error):
        notification = ToastErrorLabel(
            text=f'Error: {repr(error)}',
            text_size=(self.root.width, None),
            size_hint=(None, 24 / self.root.height),
            pos_hint={'x': 0, 'y': 0},
        )
        self.root.ids.toast_layout.add_widget(notification)

        Clock.schedule_once(lambda dt: self.root.ids.toast_layout.remove_widget(notification), 5)


class ToastErrorLabel(Label):
    pass


class LircButton(Button):
    remote_name = StringProperty('SONY_RMT_B104P')
    key_name = StringProperty('KEY_POWER')

    def __init__(self, *args, **kwargs):
        super(LircButton, self).__init__(*args, **kwargs)
        self.always_release = True

    def on_press(self):
        #print(f'on_press: {repr(self.last_touch)}')
        self.start_send()

    def on_release(self):
        #print(f'on_release: {repr(self.last_touch)}')
        self.stop_send()
        self.stop_all()

    def on_touch_down(self, touch):
        if touch.device not in TOUCH_EVENT_ALLOW_LIST:
            return False
        return super(LircButton, self).on_touch_down(touch)

    def on_touch_up(self, touch):
        if touch.device not in TOUCH_EVENT_ALLOW_LIST:
            return False
        return super(LircButton, self).on_touch_up(touch)

    #def on_touch_down(self, touch):
    #    if self.collide_point(*touch.pos):

    #        # if the touch collides with our widget, let's grab it
    #        touch.grab(self)

    #        self.start_send()

    #        # and accept the touch.
    #        return True

    #def on_touch_up(self, touch):
    #    # here, you don't check if the touch collides or things like that.
    #    # you just need to check if it's a grabbed touch event
    #    if touch.grab_current is self:

    #        # ok, the current touch is dispatched for us.
    #        self.stop_send()

    #        # don't forget to ungrab ourself, or you might have side effects
    #        touch.ungrab(self)

    #        # and accept the last up
    #        return True

    def start_send(self):
        try:
            LIRC.send_start(self.remote_name, self.key_name)
        except lirc.exceptions.LircdCommandFailureError as error:
            print(f'Unable to start sending {self.key_name}!')
            print(error)
            App.get_running_app().toast_error(error)

    def stop_send(self):
        try:
            LIRC.send_stop(self.remote_name, self.key_name)
        except lirc.exceptions.LircdCommandFailureError as error:
            print(f'Unable to stop sending {self.key_name}!')
            print(error)
            #App.get_running_app().toast_error(error)

    def stop_all(self):
        try:
            LIRC.send_stop()
        except lirc.exceptions.LircdCommandFailureError as error:
            print('Unable to stop sending last key!')
            print(error)
            #App.get_running_app().toast_error(error)


if __name__ == '__main__':
    PyRemoteApp().run()
